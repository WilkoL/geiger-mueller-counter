#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void init_peripherals(void);

volatile uint8_t one_second;
volatile uint16_t counts_per_second;

int main(void)
{	
	float logarithm_of_cps;
	float average;

	init_peripherals();
	one_second = 0;
	counts_per_second = 1;
	logarithm_of_cps = 0;
	average = 0;
		
    while (1) 
    {	
		if (one_second)
		{
			one_second = 0;									//clear message from interrupt
			
			logarithm_of_cps = log(counts_per_second);		//take logarithm of counts recorded in 1 second
			counts_per_second = 1;							//then clear that number (NOT to ZERO!)
		}
		
		
		average = (average * 127 + logarithm_of_cps) / 128;	//average it with previous measurements
		if (average < 6.0)									//less than 400 cps
		{
			OCR0B = (int) (average * 40);					//set PWM value for analog meter
			OCR0A = 255 - OCR0B;							//my analog meter has its zero in the middle
		}
		else 												//of the scale, so with no CPS it has to 
		{
			OCR0B = 255;									//be pulled low (negative current)
			OCR0A = 0;
		}
		_delay_ms(20);										//slow down the process, for a smooth meter reaction
    }
}

void init_peripherals(void)
{
	DDRB |= (1 << PB0) | (1 << PB2) | (1 << PB3) | (1 << PB4);
	DDRD |= (1 << PD5);
	
	MCUCR |= (1 << ISC01);									//generate interrupt INT0 on falling edge
	GIMSK |= (1 << INT0);									//enable interrupts on port INT0
	
	//TIM0 is used for 1 second timing and pwm for the analog meter
	TCCR0A |= (1 << COM0A1);								//Clear OC0A on Compare Match, set OC0A at TOP
	TCCR0A |= (1 << COM0B1);								//Clear OC0B on Compare Match, set OC0B at TOP
	TCCR0A |= (1 << WGM01) | (1 << WGM00);					//FAST PWM, TOP is 0xFF, overflow interrupt set on TOP
	TCCR0B |= (1 << CS02);									//prescaler on 256
	TIMSK |= (1 << TOIE0);									//interrupt on overflow (122 Hz)
	
	//TIM1 is used for generating the pulses for the High Voltage generator
	TCCR1A |= (1 << COM1A1);								//Clear OC1A on Compare Match, set OC1A at TOP
	TCCR1A |= (1 << COM1B1) | (1 << COM1B0);				//Set OC1B on Compare Match, clear OC1B at TOP
	TCCR1A |= (1 << WGM11);									//fast pwm, top = ICR1
	TCCR1B |= (1 << WGM12) | (1 << WGM13);	
	TCCR1B |= (1 << CS10);									//prescaler = 1	
	TCCR1C = 0;	
	ICR1 = 1100;											//freq circa 7000 Hz
	OCR1A = 600;											//dutycycle circa 50%
	OCR1B = OCR1A - 10;										//OC1B reacts approx 1 uS before OC1A
	
	sei();
}



ISR(INT0_vect)
{
	if (counts_per_second < 65535) counts_per_second++;		//particle detected
	
	PORTB |= (1 << PB0);									//flash led (and make small clicking noise)
	_delay_ms(5);
	PORTB &= ~(1 << PB0);
}

ISR (TIMER0_OVF_vect)										//122 times / second
{
	static uint16_t counter = 0;
	
	if (counter != 0) counter--;
	else													//once per second
	{
		counter = 121;
		one_second = 1;										//message to main
	}
}

